import React, {useState, useEffect} from 'react'
import {Modal, Button} from 'react-bootstrap'
import Todo from './Todo'

const ModalSearch = ({ children, show, setClose, todos, onMarkTodo, onSelectTodoToUpdate, onDeleteTodo }) => {
  const [matchTodo, setMatchTodo] = useState(todos)
  const [text, setText] = useState("")

  useEffect(() => {
    // lorsque la valeur de todos changes dans le grand composant
    // on le met a jours dans celui ci

    if (text === "")
      setMatchTodo(todos)
  }, [todos])

  useEffect(() => {
    // lorsque la valeur de todos changes dans le grand composant
    // on le met a jours dans celui ci

    setMatchTodo(todos)
  }, [todos.length])

  const researchTodo = () => {
    let regex = new RegExp(text, "i")

    const matchedTodos = []

    todos.forEach(todo => {
      if (todo.text.match(regex) !== null) {
        matchedTodos.push(todo)
      }
    })

    setMatchTodo(matchedTodos)
  }

  const handleCloseModal = () => {
    setClose()

    setText("")
    setMatchTodo(todos)
  }

  return (
    <Modal onHide={handleCloseModal} show={show} fade>
      <Modal.Header>
        <section className="tma-research">
          <input
            value={text}
            type="text"
            placeholder="Faite une recherche ici..."
            onChange={(event) => setText(event.target.value)}
          />
          <button className="btn btn-research" onClick={researchTodo}>VALIDER</button>
        </section>
      </Modal.Header>

      <Modal.Body>
        <section className="tma-todos-section-search">
          <div>

            {children}

            <section className="tma-todos-list-search">
              {
                matchTodo.map(todo => (
                  <Todo
                    key={todo.id}
                    todo={todo}
                    onMarkTodo={onMarkTodo}
                    onDeleteTodo={onDeleteTodo}
                    onSelectTodoToUpdate={onSelectTodoToUpdate}
                  />
                ))
              }

              {
                matchTodo.length === 0 && <div className="p-2 text-align-center w-100 d-flex flex-row justify-content-center align-items-center">Aucun résultat</div>
              }
            </section>
          </div>
        </section>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="light" style={{backgroundColor: "#eee"}} onClick={handleCloseModal}>Fermer</Button>
      </Modal.Footer>
    </Modal>
  )
}

const ModalInfo = ({ show, setClose }) => {
  return (
    <Modal onHide={setClose} show={show}>
      <Modal.Header>
        <h3>Infos</h3>
      </Modal.Header>
      <Modal.Body>
        <section className="tma-info">
          <div className="tma-info-top">
            <h3>A Propos</h3>
            <span>
              {`TMApp (Task Manager Application) est un application de gestion de tâches très intuitive, facile à utiliser et vous offrant la possibilité de bien gérer vos tâches du quotidien.`}
            </span>
          </div>
          <span>
            &copy; Copyright 2021 dilane3
          </span>
        </section>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="light" style={{backgroundColor: "#eee"}} onClick={setClose}>Fermer</Button>
      </Modal.Footer>
    </Modal>
  )
}

const ModalStats = ({ show, setClose, todos }) => {
  const evaluateStats = () => {
    const totalTodo = todos.length
    let todoEnded = 0

    todos.forEach(todo => {
      if (todo.status === "ended")
        todoEnded += 1
    })

    const str1 = `${totalTodo} ${totalTodo > 1 ? "tâches au total":"tâche au total"}`
    const str2 = `${todoEnded} ${todoEnded > 1 ? "tâches terminés":"tâche terminé"}`
    const str3 = `${totalTodo - todoEnded} ${totalTodo - todoEnded > 1 ? "tâches en cours":"tâche en cours"}`

    return {str1, str2, str3}
  }

  return (
    <Modal onHide={setClose} show={show}>
      <Modal.Header>
        <h3>Statistiques</h3>
      </Modal.Header>
      <Modal.Body>
        <section className="tma-stats">
          <span> {evaluateStats().str1}</span>
          <span> {evaluateStats().str2}</span>
          <span> {evaluateStats().str3}</span>
        </section>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="light" style={{backgroundColor: "#eee"}} onClick={setClose}>Fermer</Button>
      </Modal.Footer>
    </Modal>
  )
}

export {ModalSearch, ModalInfo, ModalStats}
