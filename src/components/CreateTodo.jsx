import React, {useState} from 'react'

const CreateTodo = ({onCreateTask}) => {
  const [text, setText] = useState("")

  const handleCreateTask = () => {
    onCreateTask(text)
    setText("")
  }

  return (
    <div className="tma-create-todo">
      <span className="mb-1">Nouvelle Tâche:</span>
      <div>
        <input
          value={text}
          type="text"
          onChange={(event) => setText(event.target.value)}
          onKeyPress={(event) => event.code === "Enter" && handleCreateTask()}
          placeholder="créer une nouvelle tâche..."
        />
        <button className="btn tma-btn" onClick={handleCreateTask}>AJOUTER</button>
      </div>
    </div>
  )
}

export default CreateTodo
