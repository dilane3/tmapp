import React, {useState, useEffect} from 'react'

const EditTodo = ({todo, onUpdateTodo}) => {
  const textValue = todo === null ? "" : todo.text
  const idValue = todo === null ? null : todo.id
  console.log(todo)
  const [text, setText] = useState(textValue)
  const [id, setId] = useState(idValue)

  useEffect(() => {
    const textValue = todo === null ? "" : todo.text
    const idValue = todo === null ? null : todo.id

    setText(textValue)
    setId(idValue)
  }, [todo])

  const reset = () => {
    if (id !== null) {
      onUpdateTodo(id, text)

      setText("")
      setId(null)
    }
  }

  return (
    <section className="tma-edit-todo">
      <span>Éditer:</span>
      <input
        value={text}
        type="text"
        onChange={(event) => setText(event.target.value)}
        placeholder="Cliquer sur l'icone 'stylo' pour éditer"
      />
      <button className={`btn tma-btn-edit ${id === null && "tma-btn-disable"}`} onClick={() => reset()}>SAUVER</button>
    </section>
  )
}

export default EditTodo
