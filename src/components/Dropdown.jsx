import React, {useState} from 'react'
import {Dropdown} from 'react-bootstrap'
import {ModalSearch, ModalInfo, ModalStats} from './Modal'
import EditTodo from './EditTodo'

const CustomToggle = React.forwardRef(({ children, onClick, show, todoToUpdate, onUpdateTodo }, ref) => (
  <span
    ref={ref}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
    className={show}
  >
    { children }
  </span>
));

function CustomDropdown({ children, show, todos, todoToUpdate, onUpdateTodo, onMarkTodo, onSelectTodoToUpdate, onDeleteTodo }) {
  const [showSearch, setShowSearch] = useState(false)
  const [showInfo, setShowInfo] = useState(false)
  const [showStats, setShowStats] = useState(false)

  return (
    <Dropdown>
      <ModalSearch
        todos={todos}
        show={showSearch}
        setClose={() => setShowSearch(false)}
        onMarkTodo={onMarkTodo}
        onDeleteTodo={onDeleteTodo}
        onSelectTodoToUpdate={onSelectTodoToUpdate}
      >
        <EditTodo todo={todoToUpdate} onUpdateTodo={onUpdateTodo} />
      </ModalSearch>

      <ModalInfo
        show={showInfo}
        setClose={() => setShowInfo(false)}
      />

      <ModalStats
        show={showStats}
        setClose={() => setShowStats(false)}
        todos={todos}
      />

      <Dropdown.Toggle
        as={CustomToggle}
        show={show}
        className="tma-icon-dropdown"
      >
        { children }
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href="#" className="dropdown-item-text" onClick={() => setShowSearch(true)}>
          <i className="bi bi-search mr-3"></i>
          <span>Recherche</span>
        </Dropdown.Item>
        <Dropdown.Item href="#" className="dropdown-item-text" onClick={() => setShowInfo(true)}>
          <i className="bi bi-info-circle mr-3"></i>
          <span>Info</span>
        </Dropdown.Item>
        <Dropdown.Item href="#" className="dropdown-item-text" onClick={() => setShowStats(true)}>
          <i className="bi bi-bar-chart mr-3"></i>
          <span>Statistiques</span>
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default CustomDropdown;
