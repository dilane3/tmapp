import React from 'react'

const Todo = ({todo, onMarkTodo, onDeleteTodo, onSelectTodoToUpdate}) => {
  const {text, id, status} = todo

  return (
    <article className="tma-todo">
      <div className="tma-todo-info">
        <input type="checkbox" onChange={(event) => onMarkTodo(event.target.checked, id)} />
        <span className={status === "ended" && "tma-todo-marked"}>
          {text}
        </span>
      </div>

      <div className="tma-todo-icons">
        {
          todo.status === "" && (
            <i className="bi bi-pen" onClick={() => onSelectTodoToUpdate(todo)}></i>
          )
        }
        <i className="bi bi-trash" onClick={() => onDeleteTodo(id)}></i>
      </div>
    </article>
  )
}

export default Todo
