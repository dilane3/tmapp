import React, {useState} from 'react';
import './App.css';
import Todo from './components/Todo';
import CreateTodo from './components/CreateTodo';
import EditTodo from './components/EditTodo';
import CustomDropdown from './components/Dropdown';

function App() {
  // state
  const [todos, setTodos] = useState([])
  const [todoToUpdate, setTodoToUpdate] = useState(null)

  // behavior
  const handleMarkTodo = (value, id) => {
    const todosClone = [...todos]

    todosClone.forEach(todo => {
      if (todo.id === id) {
        if (value)
          todo.status = "ended"
        else
          todo.status = ""
      }
    })

    setTodos(todosClone)
  }

  const handleCreateTask = (text) => {
    const todosClone = [...todos]

    if (text.length > 0) {
      let index = null

      if (todosClone.length > 0)
        index = todosClone[todosClone.length - 1].id + 1
      else
        index = 1

      const newTodo = {
        text,
        id: index,
        status: ""
      }

      todosClone.push(newTodo)

      setTodos(todosClone)
    }
  }

  const handleDeleteTask = (id) => {
    if (todoToUpdate !== null && todoToUpdate.id === id)
      setTodoToUpdate(null)

    setTodos(todos.filter(todo => todo.id !== id))
  }

  const handleUpdateTask = (id, value) => {
    const todosClone = [...todos]

    const index = todosClone.findIndex(todo => todo.id === id)

    if (index !== undefined) {
      todosClone[index].text = value

      setTodos(todosClone)
      setTodoToUpdate(null)
    }
  }

  const handleSelectTaskToUpdate = (todo) => {
    setTodoToUpdate(todo)
  }

  return (
    <div className="App">
      <header className="tma-header">
        <div className="d-flex flex-row justify-content-between w-100 align-items-center">
          <span className="tma-logo">TMApp</span>

          <CustomDropdown
            todos={todos}
            show={true}
            todoToUpdate={todoToUpdate}
            onUpdateTodo={handleUpdateTask}
            onMarkTodo={handleMarkTodo}
            onDeleteTodo={handleDeleteTask}
            onSelectTodoToUpdate={handleSelectTaskToUpdate}
          >
            <i className="bi bi-justify tma-icon-dropdown"></i>
          </CustomDropdown>
        </div>

        <CreateTodo onCreateTask={handleCreateTask} />
      </header>

      <section className="tma-todos-section">
        <div>
          <EditTodo todo={todoToUpdate} onUpdateTodo={handleUpdateTask} />

          <section className="tma-todos-list">
            {
              todos.map(todo => (
                <Todo
                  key={todo.id}
                  todo={todo}
                  onMarkTodo={handleMarkTodo}
                  onDeleteTodo={handleDeleteTask}
                  onSelectTodoToUpdate={handleSelectTaskToUpdate}
                />
              ))
            }
            {
              todos.length === 0 && <div className="p-2 text-align-center w-100 d-flex flex-row justify-content-center align-items-center">Aucune tâche</div>
            }
          </section>
        </div>
      </section>
    </div>
  );
}

export default App;
